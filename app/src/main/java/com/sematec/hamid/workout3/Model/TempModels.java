package com.sematec.hamid.workout3.Model;

/**
 * Created by Hamid on 23/08/2017.
 */

public class TempModels {
    String itemName , itemHighTemp ,itemLowTemp ;

    public TempModels() {
    }

    public TempModels(String itemName, String itemHighTemp, String itemLowTemp) {
        this.itemName = itemName;
        this.itemHighTemp = itemHighTemp;
        this.itemLowTemp = itemLowTemp;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemHighTemp() {
        return itemHighTemp;
    }

    public void setItemHighTemp(String itemHighTemp) {
        this.itemHighTemp = itemHighTemp;
    }

    public String getItemLowTemp() {
        return itemLowTemp;
    }

    public void setItemLowTemp(String itemLowTemp) {
        this.itemLowTemp = itemLowTemp;
    }
}
