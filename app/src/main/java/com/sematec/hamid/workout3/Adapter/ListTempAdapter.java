package com.sematec.hamid.workout3.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sematec.hamid.workout3.Model.TempModels;
import com.sematec.hamid.workout3.R;

import java.util.List;

/**
 * Created by Hamid on 23/08/2017.
 */

public class ListTempAdapter extends BaseAdapter {

    Context mContext;
    List<TempModels> tempList;

    public ListTempAdapter(Context mContext, List<TempModels> tempList) {
        this.mContext = mContext;
        this.tempList = tempList;
    }

    @Override
    public int getCount() {
        return tempList.size();
    }

    @Override
    public Object getItem(int position) {
        return tempList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        View rowView = LayoutInflater.from(mContext).inflate(R.layout.temp_list_item, viewGroup, false);

        TextView itemName = (TextView) rowView.findViewById(R.id.itemName);
        TextView itemHighTemp = (TextView) rowView.findViewById(R.id.itemHighTemp);
        TextView itemLowTemp = (TextView) rowView.findViewById(R.id.itemLowTemp);

        itemName.setText(tempList.get(position).getItemName());
        itemHighTemp.setText(tempList.get(position).getItemHighTemp());
        itemLowTemp.setText(tempList.get(position).getItemLowTemp());

        return rowView;
    }
}
