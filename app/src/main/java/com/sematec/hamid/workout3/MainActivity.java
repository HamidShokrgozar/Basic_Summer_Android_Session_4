package com.sematec.hamid.workout3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.BatteryManager;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;
import com.sematec.hamid.workout3.Adapter.ListTempAdapter;
import com.sematec.hamid.workout3.Model.Forecast;
import com.sematec.hamid.workout3.Model.TempModels;
import com.sematec.hamid.workout3.Model.YahooModels;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import cz.msebera.android.httpclient.Header;


public class MainActivity extends AppCompatActivity {
    ImageView background;
    TextView cityNameView, tempCity, chargeView;
    EditText newCity;
    Button citybtn;
    ListView listtemp;
    BroadcastReceiver connectreceiver, disconnectreceiver;
    String city, itemModel, fulltemp, modelTemp;
    boolean clickcitybtn = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        background = (ImageView) findViewById(R.id.background);


        cityNameView = (TextView) findViewById(R.id.cityNameView);
        tempCity = (TextView) findViewById(R.id.tempCity);
        chargeView = (TextView) findViewById(R.id.chargeView);
        newCity = (EditText) findViewById(R.id.newCity);
        citybtn = (Button) findViewById(R.id.citybtn);
        listtemp = (ListView) findViewById(R.id.listtemp);


        Calendar d = Calendar.getInstance();
        int Hour = d.getTime().getHours();

        if (Hour >= 21 & Hour < 5)
            background.setBackgroundResource(R.drawable.wallpaper_4);

        else if (Hour >= 5 & Hour < 11)
            background.setBackgroundResource(R.drawable.wallpaper_1);

        else if (Hour >= 11 & Hour < 16)
            background.setBackgroundResource(R.drawable.wallpaper_2);

        else
            background.setBackgroundResource(R.drawable.wallpaper_3);


        findViewById(R.id.chargeView).setVisibility(View.GONE);

        connectreceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                findViewById(R.id.newCity).setVisibility(View.GONE);
                findViewById(R.id.citybtn).setVisibility(View.GONE);
                findViewById(R.id.chargeView).setVisibility(View.VISIBLE);


                IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
                Intent batteryStatus = context.registerReceiver(null, ifilter);

                int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                int scale = batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1);

                float batteryPct = (level / (float) scale) * 100;

                chargeView.setText((int) batteryPct + "% Charge");


            }
        };

        IntentFilter chargeFilter = new IntentFilter("android.intent.action.ACTION_POWER_CONNECTED");

        registerReceiver(connectreceiver, chargeFilter);

        disconnectreceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                findViewById(R.id.chargeView).setVisibility(View.GONE);
                findViewById(R.id.newCity).setVisibility(View.VISIBLE);
                findViewById(R.id.citybtn).setVisibility(View.VISIBLE);
            }
        };
        IntentFilter disconnectFilter = new IntentFilter("android.intent.action.ACTION_POWER_DISCONNECTED");

        registerReceiver(disconnectreceiver, disconnectFilter);


        getCity(city);

    }


    void getCity(String city) {
        final String urlCityName = getString(R.string.ipcityname);

        AsyncHttpClient cityClient = new AsyncHttpClient();
        cityClient.get(urlCityName, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                ShowToast("Error" + throwable);
                if (!getshared(urlCityName, "-").equals("-"))
                    getCityFromJson(getshared(urlCityName, "-"));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                setshared(urlCityName, responseString);
                getCityFromJson(responseString);
            }

        });

    }

    void getCityFromJson(String cityresponse) {
        try {
            JSONObject cityobj = new JSONObject(cityresponse);

            String city = cityobj.getString("city");
            cityNameView.setText(city);


            getWeatherByAsync(city);


        } catch (Exception e) {
            ShowToast("Error" + e);
        }
    }

    void getWeatherByAsync(final String city) {
        final String urlcityTemp = getString(R.string.yahoo_weather_part1)
                + city + getString(R.string.yahoo_weather_part2);

        AsyncHttpClient tempClient = new AsyncHttpClient();
        tempClient.get(urlcityTemp, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                ShowToast("Error" + throwable);
                if (!getshared(urlcityTemp, "-").equals("-"))
                    getTempByGson(getshared(urlcityTemp, "-"));
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                setshared(urlcityTemp, responseString);

                getTempByGson(responseString);
                getListTempByGson(city);

            }
        });
    }

    void getTempByGson(String tempresponse) {
        Gson gson = new Gson();
        YahooModels tempModel = gson.fromJson(tempresponse, YahooModels.class);

        String temp = tempModel.getQuery().getResults().getChannel().getItem().getCondition().getTemp();

        setTempResult(temp);


    }

    void getListTempByGson(String listTempResponse) {


        List<TempModels> tempList = new ArrayList<>();

        for (int t = 1; t <= 10; t++) {
            Gson listGson = new Gson();
            Forecast listTempModel = listGson.fromJson(listTempResponse, Forecast.class);
            itemModel = modelTemp + String.valueOf(t);
            String day = listTempModel.getDay();
            String high = listTempModel.getHigh();
            String low = listTempModel.getLow();

            TempModels itemModel = new TempModels();
            itemModel.setItemName(day);
            itemModel.setItemHighTemp(high);
            itemModel.setItemLowTemp(low);
            tempList.add(itemModel);

        }

        ListTempAdapter adapter = new ListTempAdapter(this, tempList);


        listtemp.setAdapter(adapter);

    }

    void setTempResult(String temp) {
        double c = (Integer.parseInt(temp) - 32) / 1.8;
        int cint = (int) c;
        tempCity.setText(String.valueOf(cint));
    }

    void ShowToast(final String str) {
        android.widget.Toast.makeText(this, "str", Toast.LENGTH_SHORT).show();
    }

    void setshared(String key, String value) {
        PreferenceManager.getDefaultSharedPreferences(this).edit()
                .putString(key, value).apply();
    }

    String getshared(String key, String defValue) {
        return PreferenceManager.getDefaultSharedPreferences(this)
                .getString(key, defValue);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(connectreceiver);
        unregisterReceiver(disconnectreceiver);
    }
}
